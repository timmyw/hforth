module Main where

import Lib
import Engine
import Paths_hforth (version)
import Data.Version (showVersion)

process :: RunState -> String -> IO RunState
process (stack, dict, ws) l = do
  let s' = (stack, dict, (words l) ++ ws)
  interpretWords s'

getAndRun :: RunState -> IO ()
getAndRun s = do
  line <- getContents
  s' <- process s line
  getAndRun s'

main :: IO ()
main = do
  putStrLn $ "hforth " ++ showVersion version
  getAndRun initState
