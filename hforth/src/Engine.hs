module Engine where

import Data.Char
import Data.Tuple.Extra

type Handler = RunState -> IO RunState

data DictWord = Word String [String] | Inbuilt String Handler

type Stack = [String]
type Dictionary = [DictWord]
type Words = [String]
             
type RunState = (Stack, Dictionary, Words)

initState :: RunState
initState = ([ "123", "456" ], initialDict, [])

initialDict :: Dictionary
initialDict = [ Inbuilt "+" operationPlus
              , Inbuilt "-" operationSubtract
              , Inbuilt "*" operationMultiply
              , Inbuilt "/" operationDivide
              , Inbuilt "/." operationDivideF
              , Inbuilt "drop" inDrop
              , Inbuilt "dup" inDup
              , Inbuilt "dup2" inDup2
              , Inbuilt "swap" inSwap
              , Inbuilt "." inDot
              ]

inDot :: RunState -> IO RunState
inDot rs@(stack, _, _) = do
  mapM_ (\s -> putStr $ s ++ " ") stack 
  putStrLn ""
  return rs

inDrop :: RunState -> IO RunState
inDrop (_:ss, dict, ws) = return (ss, dict, ws)

inDup :: RunState -> IO RunState
inDup (s:ss, dict, ws) = return (s:s:ss, dict, ws)

inDup2 :: RunState -> IO RunState
inDup2 (s:s':ss, dict, ws) = return (s':s:s':ss, dict, ws)

inSwap :: RunState -> IO RunState
inSwap rs@(s:[], _, _) = internalError "Stack underflow" rs
inSwap (s:s':ss, dict, ws) = return (s':s:ss, dict, ws)

internalError :: String -> RunState -> IO RunState
internalError msg s = do
  putStrLn msg
  return s

operationPlus :: RunState -> IO RunState
operationPlus s@(stack, dict, ws) = do
  (v1, s')  <- pop s
  (v2, s'') <- pop s'
  let i = (read v1 :: Integer) + (read v2 :: Integer)
  let stack' = (show i) : (fst3 s'') 
  return (stack', dict, ws)

operationSubtract :: RunState -> IO RunState
operationSubtract rs@(stack, dict, ws) = do
  ((v1, v2), rs') <- pop2 rs
  let i = (read v1 :: Integer) - (read v2 :: Integer)
  let stack' = (show i) : (fst3 rs') 
  return (stack', dict, ws)

operationMultiply :: RunState -> IO RunState
operationMultiply rs@(stack, dict, ws) = do
  ((v1, v2), rs') <- pop2 rs
  let i = (read v1 :: Integer) * (read v2 :: Integer)
  let stack' = (show i) : (fst3 rs') 
  return (stack', dict, ws)

operationDivide :: RunState -> IO RunState
operationDivide rs@(stack, dict, ws) = do
  ((v1, v2), rs') <- pop2 rs
  let i = (read v1 :: Integer) `div` (read v2 :: Integer)
  let stack' = (show i) : (fst3 rs') 
  return (stack', dict, ws)

operationDivideF :: RunState -> IO RunState
operationDivideF rs@(stack, dict, ws) = do
  ((v1, v2), rs') <- pop2 rs
  let i = (read v1 :: Float) / (read v2 :: Float)
  let stack' = (show i) : (fst3 rs') 
  return (stack', dict, ws)
  
pop2 :: RunState -> IO ((String, String), RunState)
pop2 rs@([], _, _)   = do
  rs' <- internalError "Stack underflow" rs
  return (("", ""),  rs')
pop2 rs@(s:[], _, _) = do
  rs' <- internalError "Stack underflow" rs
  return (("", ""), rs')
pop2 (s:s':ss, dict, ws) = return ((s, s'), (ss, dict, ws))

pop :: RunState -> IO (String, RunState)
pop s@([], _, _)         = do
  s' <- internalError "Stack underflow" s
  return ("", s')
pop s@(v:vs, dict, ws)   = return (v, (vs, dict, ws))

interpretWord :: RunState -> String -> IO RunState
interpretWord s@(stack, dict, ws) w 
  | w == "......" = do
      inDot s
      return s
  | otherwise = do                 -- Try current dictionary
      iterDict s w

iterDict :: RunState -> String -> IO RunState
iterDict s@(stack, dict, ws) w = do
  (stack', _, ws') <- iterDict' s w
  return (stack', dict, ws')

iterDict' :: RunState -> String -> IO RunState
iterDict' s@(stack, [], ws) w   = return (w : stack, [], ws) -- push it onto the stack
iterDict' s@(stack, (Inbuilt d f):ds, ws) w = do
  if d == w then
    do
      f s
    else
    iterDict' (stack, ds, ws) w
    
interpretWords :: RunState -> IO RunState
interpretWords s@(_, _, []) = return s
interpretWords (stack, dict, w:ws) = do
  (stack', dict', ws') <- interpretWord (stack, dict, ws) (map toLower w)
  s' <- interpretWords (stack', dict', ws')
  return s'
  
